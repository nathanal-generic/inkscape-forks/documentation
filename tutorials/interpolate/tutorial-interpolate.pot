msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-interpolate.xml:6
msgid "Interpolate"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-interpolate.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-interpolate.xml:11
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:16
msgid "Introduction"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:17
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:22
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate</"
"guimenuitem></menuchoice> from the menu."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:27
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:36
msgid "Interpolation between two identical paths"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:37
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:42 tutorial-interpolate.xml:75
msgid "For example, take the following two paths:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:52
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:62
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:69
msgid "Interpolation between two different paths"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:70
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:85
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:95
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:99
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:105
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:116
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:126
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:146
msgid "Interpolation Method"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:147
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either Interpolation Method 1 or 2."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:152
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:162
msgid "Now compare this to Interpolation Method 1:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:172
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:178
msgid "Exponent"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:179
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:183
msgid "Here is the result of another basic example with an exponent of 0."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:193
msgid "The same example with an exponent of 1:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:203
msgid "with an exponent of 2:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:213
msgid "and with an exponent of -1:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:223
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:228
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:240
msgid "Duplicate Endpaths"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:241
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:247
msgid "Interpolate Style"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:248
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:253
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:263
msgid "Interpolate Style also affects the stroke of a path:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:273
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:285
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:286
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:298
msgid "And interpolate between the two lines to create your gradient:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:310
msgid "Conclusion"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:311
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""

#. (itstool) path: Work/format
#: interpolate-f01.svg:49 interpolate-f02.svg:49 interpolate-f03.svg:49
#: interpolate-f04.svg:49 interpolate-f05.svg:49 interpolate-f07.svg:49
#: interpolate-f08.svg:49 interpolate-f09.svg:49 interpolate-f10.svg:49
#: interpolate-f11.svg:49 interpolate-f12.svg:49 interpolate-f13.svg:49
#: interpolate-f14.svg:49 interpolate-f15.svg:49 interpolate-f16.svg:49
#: interpolate-f17.svg:49 interpolate-f18.svg:49 interpolate-f19.svg:49
#: interpolate-f20.svg:52
msgid "image/svg+xml"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:126 interpolate-f04.svg:117 interpolate-f11.svg:117
#, no-wrap
msgid "Exponent: 0.0"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:130 interpolate-f04.svg:121 interpolate-f11.svg:121
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:134 interpolate-f04.svg:125 interpolate-f11.svg:125
#, no-wrap
msgid "Interpolation Method: 2"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:138 interpolate-f04.svg:129 interpolate-f11.svg:129
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:142 interpolate-f04.svg:133 interpolate-f11.svg:133
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr ""
