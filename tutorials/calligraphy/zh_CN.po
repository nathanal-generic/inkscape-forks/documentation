msgid ""
msgstr "Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2021-10-11 17:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: 驿窗 <classenu@163.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "驿窗 <classenu@163.com>, 2022"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "书法"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr "教程"

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr "书法工具是Inkscape提供的一个出色的工具。本教程将帮助您熟悉该工具的工作原理，并演示一些书法艺术的基本技巧。"

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr "使用<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>方向键</keycap></keycombo>，<mousebutton role=\"mouse-wheel\">鼠标滚轮</mousebutton>，或者<mousebutton role=\"middle-button-drag\">鼠标中键拖动</mousebutton>可以向下滚动页面。关于创建、选择和变换对象的基础操作知识，请参考菜单<menuchoice><guimenu>帮助</guimenu><guimenuitem>教程</guimenuitem></menuchoice>中的\"基础\"教程。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "历史和风格"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr "按照字典的定义，<firstterm>书法</firstterm>的意思是\"优美的文字\"或者\"优美的书写\"。本质上，书法是一种制作漂亮或精美笔迹的艺术。听起来有些吓人，但是只要稍加练习，任何人都可以掌握书法工具的基本操作。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is on wedding invitations. There are three main styles of calligraphy:"
msgstr "直到书籍印刷设备问世之前，书籍和其他出版物的创作方式都是靠手写完成的。所有出版的书籍及其它出版物，都必须由抄写员手写。今天，普通人最有可能见到书法的地方是在结婚请柬上。书法主要有三种风格："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:32
msgid "There are three main styles of calligraphy:"
msgstr "书法主要有三种风格："

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:37
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr "西方或罗马：保存下来的文字主要是用羽毛笔和墨水写在羊皮纸或牛皮纸等材料上。"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:42
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr "阿拉伯语，包括用阿拉伯文字书写的其他语言，用芦苇笔和墨水在纸上书写。（这种风格在1440年之前产生了迄今为止保存最多的文字资料。）"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:47
msgid "Chinese or Oriental, executed with a brush."
msgstr "中国或东方，用毛笔书写。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:52
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr "这三种风格在不同的场景相互影响。古往今来使用的西方字体风格包括Rustic、Carolingian、Blackletter等。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:55
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr "软件的<guimenuitem>撤销</guimenuitem>命令是我们与过去抄写员相比的一大优势：如果您犯了错误，并不会导致整个页面都被破坏。Inkscape的书法工具还实现了一些传统笔墨无法实现的技术。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:62
msgid "Hardware"
msgstr "硬件"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:63
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr "如果您使用<firstterm>绘图板和手写笔</firstterm>(例如Wacom)，那么书法工具的效果会更明显。由于书法工具的灵活性，即使只用鼠标也可以创作一些相当复杂的书法效果，不过要产生快速的连笔效果可能会有些困难。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:68
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr "Inkscape能够利用支持这些功能的手写笔的<firstterm>压力灵敏度</firstterm>和<firstterm> 倾斜灵敏度</firstterm>。默认情况下，灵敏度功能处于禁用状态，需要手动配置后才能生效。另外，请记住，与普通毛笔不同，鹅毛书写笔或尖头书写笔对压力也不是很敏感。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr "如果您有手写板，并且想使用灵敏度功能，需要先对设备做一些设置才行。此类设置仅需执行一次即可一直有效。要启用灵敏度支持，您必须在启动Inkscape之前连接手写板，然后在<guimenu>编辑</guimenu>菜单中打开<guimenuitem>输入设备</guimenuitem>对话框。打开此对话框，您可以选择首选设备并设置手写板和手写笔。最后，在完成了这些设置之后，切换到书法工具并调整工具栏中的压力和倾斜度设置。之后，Inkscape会记住这些设置。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:83
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr "Inkscape书法工具可能对笔画的<firstterm>速度</firstterm> 敏感(请参考下面的\"细化\")，因此，如果您使用鼠标，那么您可能希望将此参数归零。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:89
msgid "Calligraphy Tool Options"
msgstr "书法工具选项"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:90
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr "按<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>，或者按<keycap>C</keycap>键，或者点击工具栏中的书法工具按钮可以激活书法工具。书法工具在其顶部工具选项栏上有8个选项：<guilabel>宽度</guilabel>&amp;<guilabel>细化</guilabel>； <guilabel>质量</guilabel>&amp;<guilabel>角度</guilabel>; <guilabel>固定</guilabel>; <guilabel>端点</guilabel>、<guilabel>抖动</guilabel>&amp;<guilabel>摆动</guilabel>。还有两个按钮可以打开和关闭手写笔压力和倾斜灵敏度(用于手写板)。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:101
msgid "Width &amp; Thinning"
msgstr "宽度与细化"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:102
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr "这两个选项控制笔迹的<firstterm>宽度</firstterm>。宽度值可以在1到100之间变化，并且(默认情况下)宽度是相对于您的编辑窗口大小的单位，与缩放无关。这是有道理的，因为书法中自然的\"度量单位\"是手的移动范围，因此，将笔尖的宽度与\"画板\"的大小保持恒定比例是很方便的，一些物理单位反而不方便。但是，此行为是以修改的，因此对于那些无论如何缩放都希望使用绝对单位的用户，可以修改此行为。如果想切换此行为模式，请使用\"工具选项栏\"上宽度数值框右侧的下拉菜单进行切换，下拉菜单中的\"%\"表示相对于窗口尺寸。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:111
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr "笔划宽度是会经常变化的参数，您无需每次都到工具栏中修改，直接使用<keycap>左方向键</keycap>和<keycap>右方向键</keycap>或使用支持压力感应功能的手写板即可进行调整。这些快捷键的方便之处在于它们可以在绘制过程中起作用，因此可以在绘制的过程中随时改变笔划的宽度："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:124
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr "笔划的宽度也可能取决于书写的速度，由<firstterm>细化</firstterm>参数控制。此参数的取值范围为-100到100；零表示宽度与速度无关，正值使较快的笔划变细，负值使较快的笔划变宽。默认值为10表示快速笔划的适度细化。以下是一些示例，所有示例均以宽度=20和角度=90绘制："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:137
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr "有趣的是，将\"宽度\"和\"细化\"都设置为100(最大)，并以极快速的动作绘制，可以获得奇怪的自然的类似神经元的形状："

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:150
msgid "Angle &amp; Fixation"
msgstr "角度和固定"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:151
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr "除了宽度，<firstterm>角度</firstterm>是最重要的书法工具参数。它是您的笔的角度，以度为单位，从0(水平)到90(逆时针垂直)或-90(顺时针垂直)变化。请注意，如果您为手写板启用了倾斜灵敏度功能，则角度参数会显示为灰色，表示角度由手写笔的倾斜度确定。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:164
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr "每种传统书法风格都有其自己常用的执笔角度。例如，Uncial字体使用25度的角度。更复杂的手法和更有经验的书法家通常会在绘图过程中改变执笔角度，Inkscape通过按 <keycap>上方向键</keycap>和<keycap>下方向键</keycap>或使用支持倾斜灵敏度的平板电脑来实现这一特性。但是，对于刚开始学习书法课程的人来说，保持角度恒定是最好的。以下是以不同角度(固定=100)绘制的笔划的示例："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:178
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr "如您所见，笔划在平行于其角度绘制时最细，垂直时最宽。正值角度是惯用右手书法的最自然和最传统的角度。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:182
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr "最薄和最厚之间的对比度水平由<firstterm>固定</firstterm>参数控制。值100表示​​角度总是恒定的，如同在\"角度\"中设置的那样。减小\"固定\"值会使笔向笔划方向转动一点。固定=0时，笔尖可自由旋转以始终垂直于笔划方向，并且角度不再起作用："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:195
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr "从印刷角度来看，最大的固定值会产生最大的笔划宽度对比(上图左首)，这是早期衬线字体(例如Times或Bodoni)的特征(因为这些字体在历史上是固定笔书法的模仿)。另一方面，固定和宽度都设置为零时(上图右首)，相当于现代的无衬线字体，例如Helvetica。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:203
msgid "Tremor"
msgstr "抖动"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:204
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr "<firstterm>抖动</firstterm>旨在让书法笔划看起来更自然。抖动在工具选项栏中是可调的，其值的范围为0到100。它会影响笔划，可以生成从轻微的不均匀到无法预料的斑点和污点的各种效果。这极大地扩展了该工具的创意可能。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:219
msgid "Wiggle &amp; Mass"
msgstr "摆动和质量"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:220
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr "与宽度和角度不同，摆动和质量这两个参数更倾向于表达书法工具的\"操作感觉\"，而不是只关注书法的视觉效果。因此，这两个工具没办法配任何演示插图。您需要自己动手尝试一下，以找到所谓的\"操作感觉\"。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:225
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr "<firstterm>摆动</firstterm>反应的是纸张对笔尖移动所造成的阻力。默认值为最小值(0)，增大此参数会使笔尖在纸张上\"打滑\"：如果质量很大，则笔会在转弯时失去控制；如果质量为零，则高摆动值会使笔更剧烈地摆动。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:230
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr "在物理学中，<firstterm>质量</firstterm>是导致惯性的原因； Inkscape书法工具的质量数值越高，笔迹就越落后于鼠标指针，并且越能平滑笔划中的急转弯和偶然的跳动。默认情况下，此值非常小(2)，因此该工具表现快速且响应迅速，但是您可以增加质量以使笔变得更慢，从而导致笔迹也更平滑。"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:238
msgid "Calligraphy examples"
msgstr "书法工具实例"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:239
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr "您已经了解了书法工具的基本功能，现在，您可以尝试创建一些书法效果。如果您是新手，请给自己买一本好的书法书，并用Inkscape进行学习。本节将仅向您展示一些简单的示例。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:244
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr "首先，要书写字母，您需要创建一些参考线来进行辅助。如果要用倾斜体或草书体书写，那么还要在辅助线中添加一些倾斜的参考线，例如："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:255
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr "然后放大，使辅助线之间的高度与您最自然的手部移动范围相对应，调整宽度和角度，然后开始！"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:259
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr "作为书法初学者，可能您要做的第一件事就是练习字母书写的基本笔划元素 - 竖和横，圆弧，撇和奈。以下是Uncial手写体字母的一些笔划元素："

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:271
msgid "Several useful tips:"
msgstr "几个有用的技巧："

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:276
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr "如果您的手在手写板上感觉很舒适，请不要移动手写板的位置，而是应该在完成每个字母后用左手滚动画布(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>方向键</keycap></keycombo>)。"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr "如果您的最后一笔不好看，只需撤消它即可(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>)。但是，如果它的形状很好但位置或大小略有不足，那么最好的处理方式是暂时切换到选择工具(<keycap>空格键</keycap>)并根据实际情况轻推/缩放/旋转它(使用<mousebutton>鼠标</mousebutton>操作或快捷键操作)，然后再次按 <keycap>空格键</keycap>切换回书法工具。"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:292
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr "完成一个单词后，再次切换到选择工具，调整均匀度和字母间距。但是，请不要让整个单词过分整齐：好的书法必须保留不规则的手写效果。一定要抵制住字母复制和笔划元素复制的诱惑；最好每一笔每一划都是独一无二的。"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:299
msgid "And here are some complete lettering examples:"
msgstr "下面是一些完整的书写示例："

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:311
msgid "Conclusion"
msgstr "总结"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:312
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr "书法不仅有趣，还是一门崇高的精神艺术，了解书法可能会改变您对周围事物的看法。Inkscape的书法工具这里只做了简要的介绍。但是，它非常有意思，可能在实际设计中也非常有用。祝您玩得愉快！"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "宽度=1，增长....                       到达47，减小...                                  回到0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "细化=0(统一宽度) "

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "细化=10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "细化=40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, no-wrap
msgid "thinning = −20"
msgstr "细化=-20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, no-wrap
msgid "thinning = −60"
msgstr "细化=-60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "角度=90度"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "角度=30(默认)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "角度=0"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, no-wrap
msgid "angle = −90 deg"
msgstr "角度=-90度"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "角度=30"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "角度=60"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "角度=90"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "角度=15"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, no-wrap
msgid "angle = −45"
msgstr "角度=-45"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "固定=100"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "固定=80"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "固定=0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "缓慢"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "中等"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "快速"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "抖动=0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "抖动=10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "抖动=30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "抖动=50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "抖动=70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "抖动=90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "抖动=20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "抖动=40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "抖动=60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "抖动=80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "抖动=100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "Uncial手写体"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Carolingian手写体"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "哥德体"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "法国哥德体"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "花斜体"
