msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2021-10-11 17:29+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: Hleb Valoshka <375gnu@gmail.com>\n"
"Language-Team: belarusian <i18n@mova.org>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Belarusian\n"
"X-Poedit-Country: BELARUS\n"
"X-Poedit-SourceCharset: utf-8\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Глеб Валошка  <375gnu@gmail.com>, 2010"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Каліґрафія"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Адзін з найвялікшых інструмэнтаў, наяўных у Inkscape — гэта інструмэнт "
"«Каліґрафія». Гэты падручнік дапаможа вам пазнаёміцца з тым, як працуе гэты "
"інструмэнт, а таксама пакажа некаторыя асноўныя мэтады мастацтва каліґрафіі."

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Выкарыстоўвайце <keycap>Ctrl+стрэлкі</keycap>, <keycap>кола мышы</keycap>, "
"ці <keycap>перацягваньне сярэдняй кнопкай мышы</keycap>, каб пракручваць "
"гэтую старонку долу. Асновы стварэньня, вылучэньня й ператварэньня аб'ектаў "
"глядзіце ў «Асновах» у <command>Даведка &gt; Падручнікі</command>."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "Гісторыя й стылі"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Паводле азначэньня слоўнікаў <firstterm>каліґрафія</firstterm> значыць "
"«прыгожае напісаньне» ці «выразны ці элеґантны почырк». Па сутнасьці, "
"каліґрафія зьяўляецца мастацтвам стварэньня прыгожага ці элеґантнага "
"пісаньня ўручную. Гэта можа гучаць пужаючы, але, крыху папрактыкаваўшыся, "
"кожны можа засвоіць асновы гэтага мастацтва."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is on wedding invitations. There are three main styles of calligraphy:"
msgstr ""
"Найранейшыя формы каліґрафіі датуюцца рысункамі пячорнага чалавека. "
"Прыблізна да 1440 г. н.э., пакуль ня быў вынайдзены друкарскі станок, "
"каліґрафія выкарыстоўвалася пры выданьні кніг ды іншых публікацый. "
"Перапісчыкі мусілі ўручную перапісваць асобна кожны асобнік кожнай кнігі ці "
"публікацыі. Яны пісалі гусіным пяром і чарнілам па пэргамэнце. Стылі літар, "
"што выкарыстоўваліся ў розныя часы, былі: Просты, Каралінскі, Ґатычны ды "
"інш. Напэўна, найчасьцей звычайны сучасны чалавек сутыкаецца з каліґрафіяй "
"на вясельных запрашэньнях."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:32
msgid "There are three main styles of calligraphy:"
msgstr "Ёсьць тры галоўныя стылі каліґрафіі:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:37
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:42
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:47
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "Кітайская ці ўсходняя"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:52
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:55
#, fuzzy
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"Адна значная перавага над напісаньнем мінуўшчыны, якую мы маем, — загад "
"<command>Адмяніць</command>: калі зробіце памылку, уся старонка ня будзе "
"сапсаваная. Інструмэнт каліґрафіі Inkscape таксама задзейнічае пэўныя "
"тэхнічныя прыёмы, якія будуць немагчымыя з традыцыйнымі пяром-і-чарнілам."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:62
msgid "Hardware"
msgstr "Абсталяваньне"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:63
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"Найлепшыя вынікі атрымоўваюцца, калі працуеце з <firstterm>пляншэтам і "
"пяром</firstterm> (напр., Wacom). Але дзякуючы гнуткасьці нашага "
"інструмэнта, нават тыя, хто мае толькі мыш, могуць зрабіць даволі складаную "
"каліґрафію, хаця й будуць пэўныя праблемы з атрыманьнем хуткіх шырокіх "
"штрыхоў."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:68
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape можа выкарыстоўнаць <firstterm>адчувальнасьць да націсканьня</"
"firstterm> й <firstterm>адчувальнасьць да нахілу</firstterm> пяра пляншэта, "
"які падтрымлівае гэтыя магчымасьці. Функцыі адчувальнасьці спачатку "
"абязьдзейненыя, бо ім патрэбныя настаўленьні. Таксама майце на ўвазе, што "
"каліґрафія зь пяром ці вострай асадкай ня вельмі адчувальная да націсканьня, "
"у адрозьненьні ад пэндзля."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Калі маеце пляншэт і хочаце карыстацца магчымасьцямі адчувальнасьці, то "
"мусіце наставіць сваю прыладу. Гэтыя настаўленьні патрэбна зрабіць толькі "
"аднойчы, і яны будуць захаваныя. Каб задзейнічаць падтрымку вы мусіце "
"падлучыць пляншэт да запуску Inkscape, а потым адкрыць дыялёґ "
"<firstterm>Прылады ўводжаньня…</firstterm> ў мэню <emphasis>Файл</emphasis>. "
"З дапамогай гэтага дыялёґу можна выбраць пажаданую прыладу й настаўленьні "
"пяра вашага пляншэту. Нарэшце, выбраўшы гэтыя настаўленьні, пераключыцеся на "
"інструмэнт каліґрафіі й націсьніце на кіроўнай панэлі ґузікі націску й "
"нахілу. З гэтага моманту Inkscape будзе памятаць гэтыя настаўленьні."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:83
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Каліґрафічнае пяро Inkscape можа быць адчувальным да <firstterm>хуткасьці</"
"firstterm> штрыхоў (гл. «Патанчэньне» ніжэй), таму, калі карыстаецеся мышай, "
"лепей абнуліце гэты парамэтар."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:89
msgid "Calligraphy Tool Options"
msgstr "Выборы інструмэнту «Каліґрафія»"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:90
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"Пераключыценя на інструмэнт каліґрафіі, націснуўшы <keycap>Ctrl+F6</keycap>, "
"клявішу <keycap>C</keycap> ці пстрыкнуўшы па ейным ґузіку на панэлі "
"інструмэнтаў. На верхняй панэлі можаце заўважыць 8 выбораў: таўшчыня й "
"патанчэньне, абмежаваньне, шапкі, дрыжэньне, гайданьне й маса. Таксама ёсьць "
"два ґузікі, які ўключаюць ці выключаюць адчувальнасьць да націску й нахілу "
"(пры працы з пляншэтамі)."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:101
msgid "Width &amp; Thinning"
msgstr "Таўшчыня й патанчэньне"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:102
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Гэтая пара выбораў кіруе <firstterm>таўшчынёй</firstterm> пяра. Яна можа "
"зьмяняцца ад 1 да 100 і (прадвызначана) вымяраецца адзінкамі, прапарцыйнымі "
"памеру вакна рысаваньня, але незалежнымі ад маштабу. Гэта мае сэнс, бо "
"натуральная «адзінка вымярэньня» ў каліґрафіі — дыяпазон рухаў вашае рукі, а "
"таму зручна мець таўшчыню пяра нязьменнай часткай памеру вашае «дошкі для "
"рысаваньня», а ня ў нейкіх сапраўдных адзінках, якія зробяць яе залежнай ад "
"маштабу. Аднак такія паводзіны неабавязковыя, і могуць быць зьмененыя тымі, "
"хто аддае перавагу абсалютным адзінкам, не зьвяртаючы ўвагу на маштаб. Каб "
"пераключыцца на такі рэжым, выкарыстоўвайце адпаведную «птушачку» на "
"старонцы настаўленьняў інструмэнта (яе можна адкрыць падвойнай пстрычкай па "
"ґузіку інструмэнту)."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:111
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Паколькі таўшчыня пяра часта зьмяняецца, яе можна папраўляць не пераходзяцы "
"да кіроўнай панэлі, выкарыстоўваючы клявішы стрэлак <keycap>управа</keycap> "
"й <keycap>улева</keycap>, ці з дапамогай пляншэта, які падтрымлівае функцыі "
"адчувальнасьці да націску. Найлепшая рэч з гэтым клявішамі, што яны працуюць "
"калі вы рысуеце, таму таўшчыню пяра можна зьмяняць у сярэдзіне штрыха:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:124
#, fuzzy
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Таўшчыня пяра можа таксама залежыць ад хуткасьці, яна кіруецца парамэтрам "
"<firstterm>патанчэньне</firstterm>. Гэты парамэтар прымае значэньні ад -100 "
"да 100, нуль значыць, што таўшчыня не залежыць ад хуткасьці, дадатныя "
"значэньні робяць хуткія штрыхі тонкімі, адмоўныя — тоўстымі. Прадвыначанае "
"0,1 задае ўмеранае патанчэньне хуткіх штрыхоў. Вось некалькі прыкладаў, усе "
"нарысаваныя з таўшчынёй=20 і вуглом=90:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:137
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Для пацехі задайце таўшчыню й патанчэньне роўнымі 100 (максымум) і рысуйце "
"рэзкімі рыўкамі, каб атрымаць надзіва натуральныя, нэўронападобныя фіґуры:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:150
msgid "Angle &amp; Fixation"
msgstr "Вугал і абмежаваньне"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:151
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Пасьля таўшчыні самым важным парамэтрам каліґрафіі зьяўляецца "
"<firstterm>вугал</firstterm>. Гэта вугал вашага пяра ў ґрадусах, зьменны ад "
"0 (гарызантальны) да 90 (вэртыкальны супраць стрэлкі гадзіньніка) ці -90 "
"(вэртыкальны за стрэлкай). Занатуйце, што калі вы задзейнічаеце "
"адчувальнасьць пляншэту да нахілу, то парамэтар вугла будзе паказвацца "
"шэрым, і вугал будзе вызначацца нахілам пяра."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:164
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Кожны традыцыйны стыль каліґрафіі мае свай уласны пераважны вугал пяра. "
"Напрыклад, унцыял выкарыстоўвае вугал 25 ґрадусаў. Больш складаныя шрыфты й "
"больш вопытныя каліґрафы часта зьмяняюць вугал падчас рысаваньня, і Inkscape "
"робіць гэта магчымым, ці праз націсканьне клявіш стрэлак <keycap>уверх</"
"keycap> і <keycap>ўніз</keycap>, ці з пляншэтам, які патдрымлівае "
"магчымасьць адчувальнасьці да нахілу. Для пачатковых урокаў каліґрафіі, "
"аднак, найлепей трымаць вугал нязьменным. Вось прыклады штрыхоў, нарысаваных "
"з рознымі вугламі (абмежаваньне=100):"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:178
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Як можаце заўважыць, штрых найтанчэйшы, калі рысуецца паралельна свайму "
"вуглу, і найтаўсьцейшы, калі рысуецца прастастаўна. Дадатныя вуглы найбольш "
"натуральныя й традыцыйныя для праварукай каліґрафіі."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:182
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Узровень кантрасту між найтанчэйшым і найтаўсьцейшым кіруецца парамэтрам "
"<firstterm>абмежаваньне</firstterm>. Значэньне 100 значыць, што вугал заўжды "
"нязьменны, роўны зададзенаму ў полі Вугал. Зьмяншэньне абмежаваньня дазваляе "
"пяру павернуцца крыху супраць напрамку штрыха. З абмежаваньнем=0 пяро вольна "
"паварочваецца, каб заўжды быць прастастаўным да штрыха, і Вугал больш ня мае "
"эфэкту:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:195
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Кажучы па-друкарску, найбольшае абмежаваньне, а таму й найбольшы кантраст "
"таўшчыні штрыха (уверсе зьлева) уласьцівы антычнай ґарнітуры з засечкамі, "
"такой як Таймз ці Бадоні (бо гэтыя ґарнітуры гістарычна былі ўдаваньнем "
"каліґрафіі з нязьменным пяром). Нулявое абмежаваньне й нулявы кантраст "
"таўшчыні (уверсе справа), зь іншага боку, наводзяць на думку пра сучасныя "
"ґарнітуры без засечак, такія як Гельвэтыка."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:203
msgid "Tremor"
msgstr "Дрыжэньне"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:204
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Дрыжэньне</firstterm> прызначанае надаваць больш натуральны "
"выгляд каліґрафічным штрыхам. Дрыжэньне рэґулюецца на кіроўнай панэлі "
"значэньнямі ад 0,0 да 0,1. Яно ўзьдзейнічае на вашыя штрыхі, ствараючы ўсё, "
"што заўгодна, ад лёгкай няправільнасьці да неверагодных кляксаў і плямаў. "
"Гэта значна пашырае творчы дыяпазон інструмэнту."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:219
msgid "Wiggle &amp; Mass"
msgstr "Гайданьне й маса"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:220
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"У адрозьненьне ад таўшчыні й вугла, апошнія два парамэтры вызначаюць як "
"інструмэнт «адчувае», а не ўплывае на візуальны вынік. Таму ў гэтым "
"разьдзеле ня будзе аніякіх рысункаў, проста паспрабуйце іх уласнаручна, каб "
"лепей зразумець."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:225
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Гайданьне</firstterm> — гэта супраціўленьне паперы руху пяра. "
"Прадвызначана найменшае (0), пры павелічэньні гэтага парамэтру папера "
"робіцца «сьлізкай»: калі маса вялікая, пяро спрабуе ўцячы на рэзкіх "
"паваротах, калі маса нулявая, высокае гайданьне прымушае пяро дзіка гайдацца."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:230
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"У фізыцы <firstterm>маса</firstterm> — гэта тое, што абумоўлівае інэрцыю, "
"чым большую масу мае інструмэнт каліґрафіі Inkscape, тым больш ён адстае ад "
"курсора мышы й больш згладжвае рэзкія павароты й хуткія штуршкі вашых "
"штрыхоў. Прадвызначана гэтае значэньне даволі малое (2), таму інструмэнт "
"хуткі й чуйны, але можна павялічыць масу каб атрымаць больш павольнае й "
"гладкае пяро."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:238
msgid "Calligraphy examples"
msgstr "Прыклады каліґрафіі"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:239
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Цяпер, ведаючы асноўныя магчымасьці інструмэнту, вы можаце паспрабаваць "
"стварыць нейкую сапраўдную каліґрафію. Калі вы навічок у гэтым мастацтве, "
"набыйце сабе добрую кніжку з каліґрафіі й вывучайце яе з дапамогай Inkscape. "
"У гэтым разьдзеле  вам будуць паказаныя некалькі простых прыкладаў."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:244
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Найперш, каб рабіць літары, вам патрэбна стварыць пару лінеек, якія будуць "
"накіроўваць вас. Калі вы зьбіраецеся пісаць нахіленым шрыфтам ці курсівам, "
"таксама дадайце некалькі нахіленых накіроўных празь дзьве лінейкі, напрыклад:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:255
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Пасьля наблізьце ці аддальце рысунак гэтак, каб вышыня між лінейкамі "
"адпавядала вашаму самаму натуральнаму дыяпазону руху рукі, адрэґулюйце "
"таўшчыню й вугал, і паехалі!"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:259
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Напэўна, першыя рэчы, якія вы мусіце зрабіць як пачатковец у каліґрафіі, — "
"папрактыкавацца з асноўнымі элемэнтамі літар: вэртыкальнымі й "
"гарызантальнымі, нахіленымі й скругленымі. Вось некалькі элемэнтаў літар "
"Унцыялу:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:271
msgid "Several useful tips:"
msgstr "Колькі карысных парад:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:276
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Калі вашая рука камфортна ляжыць на пляншэце, не перасоўвайце яе. Замест "
"гэтага перасоўвайце палатно (клявішы <keycap>Ctrl+стрэлкі</keycap>) левай "
"рукой, скончыўшы кожную літару."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Калі апошні штрых кепскі, то проста адмяніце яго (<keycap>Ctrl+Z</keycap>). "
"Аднак, калі ягоная форма добрая, але становішча ці памер небездакорныя, то "
"лепей часова пераключыцца на «Вылучальнік» (<keycap>Прабел</keycap>) і "
"пасунуць/зьмяніць памер/паварот як трэба, пасьля ізноў націсьніце "
"<keycap>Прабел</keycap> і вярніцеся да інструмэнту каліґрафіі."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:292
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Зрабіўшы слова, ізноў пераключыцеся на «Вылучальнік», каб паправіць "
"аднастайнасьць асноўных штрыхоў і міжлітарныя інтэрвалы. Аднак не "
"перастарайцеся, добрая каліґрафія мусіць захоўваць няправільны выгляд, нібы "
"зроблены ўручную. Не паддавайцеся спакусе капіяваць літары ці іхныя "
"элемэнты, кожны штрых мусіць быць арыґінальным."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:299
msgid "And here are some complete lettering examples:"
msgstr "І вось некалькі прыкладаў напісаньня:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:311
msgid "Conclusion"
msgstr "Высновы"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:312
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"Каліґрафія — гэта ня толькі забава, гэта глыбока духоўнае мастацтва, якое "
"можа зьмяніць ваш погляд на ўсё, што вы робіце й бачыце. Інструмэнт "
"каліґрафіі Inkscape можа служыць толькі сьціплымі ўводзінамі. Але, усё-ткі "
"ён вельмі добры для забаў і можа прыдасца ў сапраўдным дызайне. Атрымоўвайце "
"асалоду!"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "таўшчыня=1, расьце…                        дасягае 47, зьмяншаецца…                               ізноў да 0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "патанчэньне = 0 (нязьменная шырыня)"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "патанчэньне = 10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "патанчэньне = 40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "патанчэньне = -20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "патанчэньне = -60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "вугал = 90 ґрад"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "вугал = 30 (прадвызначаны)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "вугал = 0"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "вугал = 90 ґрад"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "вугал = 30"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "вугал = 60"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "вугал = 90"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "вугал = 15"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "вугал = -45"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "абмежаваньне = 100"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "абмежаваньне = 80"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "абмежаваньне = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "паволі"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "сярэдне"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "хутка"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "дрыжэньне = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "дрыжэньне = 10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "дрыжэньне = 30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "дрыжэньне = 50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "дрыжэньне = 70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "дрыжэньне = 90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "дрыжэньне = 20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "дрыжэньне = 40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "дрыжэньне = 60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "дрыжэньне = 80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "дрыжэньне = 100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "Унцыял"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Каралінскі шрыфт"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "Ґатычны шрыфт"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "Бастарда"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Пышны курсіў"

#~ msgid "Western or Roman"
#~ msgstr "Заходняя ці раманская"

#~ msgid "Arabic"
#~ msgstr "Арабская"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Гэты падручнік факусуецца, збольшага, на заходняй каліґрафіі, бо іншыя "
#~ "два стылі выкарыстоўваюць пэндзаль (а ня вострае пяро), што не адпавядае "
#~ "таму, як працуе наш інструмэнт каліґрафіі."

#~ msgid "angle = -90 deg"
#~ msgstr "вугал = -90 ґрад"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "буля бяк, buliabyak@users.sf.net і josh andler, scislac@users.sf.net"
