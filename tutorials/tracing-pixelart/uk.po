# Translation of Inkscape's tracing pixelart tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2018-06-03 19:55+0300\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>, 2018\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2018"

#. (itstool) path: articleinfo/title
#: tutorial-tracing-pixelart.xml:6
msgid "Tracing Pixel Art"
msgstr "Векторизація піксельної графіки"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing-pixelart.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:11
msgid "Before we had access to great vector graphics editing software..."
msgstr ""
"В часи, коли чудове програмне забезпечення з редагування векторної графіки "
"було ще недоступним…"

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:14
msgid "Even before we had 640x480 computer displays..."
msgstr ""
"Навіть, ще до появи комп'ютерних моніторів з роздільною здатністю 640×480…"

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:17
msgid ""
"It was common to play video games with carefully crafted pixels in low "
"resolutions displays."
msgstr ""
"Було нормою грати у відеоігри з ретельно обробленими пікселями на моніторах "
"з низькою роздільною здатністю."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:20
msgid "We name \"Pixel Art\" the kind of art born in this age."
msgstr ""
"«Піксельною графікою» ми називаємо вид графіки, що з'явився в цей період."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:24
msgid ""
"Inkscape is powered by <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> with the ability to automatically vectorize these "
"\"special\" Pixel Art images. You can try other types of input images too, "
"but be warned: The result won't be equally good and it is a better idea to "
"use the other Inkscape tracer, potrace."
msgstr ""
"«Inkscape» оснащено «<ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink>» зі здатністю автоматичної векторизації цих "
"«спеціальних» картинок векторної графіки. Можна також спробувати інші типи "
"вхідних зображень, але будьте обачні: результат не буде таким же вдалим і "
"тому краще використовувати інший трасувальник «Inkscape» – «Potrace»."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:30
msgid ""
"Let's start with a sample image to show you the capabilities of this tracer "
"engine. Below there is an example of a raster image (taken from a Liberated "
"Pixel Cup entry) on the left and its vectorized output on the right."
msgstr ""
"Давайте розпочнемо зі зразка зображення, щоб показати можливості цього рушія "
"векторизації. Внизу – приклад растрового зображення (взято з «Liberated "
"Pixel Cup») зліва та векторизований результат – справа."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:41
msgid ""
"libdepixelize uses Kopf-Lischinski algorithm to vectorize images. This "
"algorithm uses ideas of several computer science techniques and math "
"concepts to produce a good result for pixel art images. One thing to notice "
"is that the alpha channel is completely ignored by the algorithm. "
"libdepixelize has currently no extensions to give a first-class citizen "
"treatment for this class of images, but all pixel art images with alpha "
"channel support are producing results similar to the main class of images "
"recognized by Kopf-Lischinski."
msgstr ""
"Для векторизації зображень, «libdepixelize» використовує алгоритм Кофа-"
"Ліщинського (Kopf-Lischinski). Цей алгоритм використовує ідеї декількох "
"технік інформатики і математичних принципів для отримання задовільного "
"результату для зображень піксельної графіки. Єдине зауваження: канал "
"прозорості повністю ігнорується алгоритмом. Наразі, «libdepixelize» не має "
"додатків для отримання першокласної обробки для такого класу зображень, але "
"всі зображення піксельної графіки з підтримкою каналу прозорості дають "
"результати подібні до основного класу зображень, що розпізнаються алгоритмом "
"Кофа-Ліщинського."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:55
msgid ""
"The image above has alpha channel and the result is just fine. Still, if you "
"find a pixel art image with a bad result and you believe that the reason is "
"the alpha channel, then contact libdepixelize maintainer (e.g. fill a bug on "
"the project page) and he will be happy to extend the algorithm. He can't "
"extend the algorithm if he don't know what images are giving bad results."
msgstr ""
"Зображення вгорі містить канал прозорості і результат якраз чудовий. Однак, "
"якщо Ви знайдете зображення піксельної графіки з незадовільним результатом і "
"якщо Ви вважаєте, що причиною цього є канал прозорості, то зв'яжіться з тим, "
"хто підтримує «libdepixelize» (наприклад, повідомте про помилку на сторінці "
"проекту) і він з радістю розширить можливості алгоритму. Неможливо покращити "
"алгоритм, не знаючи, які саме зображення дають поганий результат."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:61
#, fuzzy
msgid ""
"The image below is a screenshot of <guimenuitem>Pixel art</guimenuitem> "
"dialog in the English localisation. You can open this dialog using the "
"<menuchoice><guimenu>Path</guimenu><guisubmenu>Trace Bitmap</"
"guisubmenu><guimenuitem>Pixel art</guimenuitem></menuchoice> menu or right-"
"clicking on an image object and then <guimenuitem>Trace Bitmap</guimenuitem>."
msgstr ""
"Зображення внизу – знімок екрану діалогу «<command>Трасування растрової "
"графіки…</command>» в українській локалізації. Цей діалог можна відкрити "
"через меню <command>«Контур» &gt; «Трасування растрової графіки…».</command> "
"або клацнувши правою кнопкою по зображенню і вибравши «<command>Трасування "
"растрової графіки</command>»."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:74
msgid ""
"This dialog has two sections: Heuristics and output. Heuristics is targeted "
"at advanced uses, but there already good defaults and you shouldn't worry "
"about that, so let's leave it for later and starting with the explanation "
"for output."
msgstr ""
"Це діалогове вікно має два розділи: «Евристика» та «Вивід». «Евристика» "
"націлена на досвідчене використання, втім там вже задовільні значення за "
"замовчуванням і можна не турбуватися за них, тому, давайте залишимо їх на "
"потім і почнемо з пояснення результату."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:79
msgid ""
"Kopf-Lischinski algorithm works (from a high-level point of view) like a "
"compiler, converting the data among several types of representation. At each "
"step the algorithm has the opportunity to explore the operations that this "
"representation offers. Some of these intermediate representations have a "
"correct visual representation (like the reshaped cell graph Voronoi output) "
"and some don't (like the similarity graph). During development of "
"libdepixelize users kept asking for adding the possibility of export these "
"intermediate stages to the libdepixelize and the original libdepixelize "
"author granted their wishes."
msgstr ""
"Алгоритм Кофа-Ліщинського працює (з високорівневого погляду) як компілятор, "
"конвертуючи дані між декількома типами представлення. На кожному етапі, "
"алгоритм має змогу дослідити операції, які пропонуються цим представленням. "
"Деякі з цих проміжних представлень мають коректне візуальне представлення "
"(як результати видозміненого результату клітинного графу Вороного "
"(Voronoi)), а деякі – ні (як граф подібності). Під час розробки "
"«libdepixelize», користувачі прохали додати можливість експорту цих "
"проміжних рівнів до «libdepixelize» і автор першої версії «libdepixelize» "
"виконав їхнє прохання."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:87
#, fuzzy
msgid ""
"The default output should give the smoothest result and is probably what you "
"want. You saw already the default output on the first samples of this "
"tutorial. If you want to try it yourself, just open the <guimenuitem>Trace "
"Bitmap</guimenuitem> dialog, select <guimenuitem>Pixel art</guimenuitem> tab "
"and click in <guibutton>OK</guibutton> after choosing some image on Inkscape."
msgstr ""
"Результат за замовчуванням має бути найплавнішим і, напевне, таким як це "
"потрібно. Ви вже бачили результат за замовчуванням в перших прикладах цього "
"уроку. Якщо бажаєте, то можете спробувати зробити це самі, просто відкрийте "
"діалогове вікно «<command>Трасування растрової графіки…</command>» і "
"клацніть «<command>Гаразд</command>» після вибору зображень в «Inkscape»."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:93
msgid ""
"You can see the Voronoi output below and this is a \"reshaped pixel image\", "
"where the cells (previously pixels) got reshaped to connect pixels that are "
"part of the same feature. No curves will be created and the image continues "
"to be composed of straight lines. The difference can be observed when you "
"magnify the image. Previously pixels couldn't share a edge with a diagonal "
"neighbour, even if it was meant to be part of the same feature. But now "
"(thanks to a color similarity graph and the heuristics that you can tune to "
"achieve a better result), it's possible to make two diagonal cells share an "
"edge (previously only single vertices were shared by two diagonal cells)."
msgstr ""
"Внизу можна побачити результат Вороного і ось – «видозмінене піксельне "
"зображення», де клітинки (попередньо, пікселі) змінили форму, щоб з'єднати "
"пікселі, які є частиною одного елементу. Жодних кривих не буде створено і "
"зображення все ще буде складатися з прямих ліній. Різницю можна помітити "
"збільшивши зображення. Раніше, пікселі не можуть мати спільні ребра з "
"сусідами по діагоналі, навіть, якщо він є частиною того ж елементу. Але "
"зараз (завдяки графу подібності кольорів та евристиці, яку можна "
"підлаштовувати, щоб отримати кращі результати), можна створювати дві "
"діагональні клітинки зі спільним ребром (раніше, лише одиничні вершини "
"розділялися між двома діагональними клітинками)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:109
msgid ""
"The standard B-splines output will give you smooth results, because the "
"previous Voronoi output will be converted to quadratic Bézier curves. "
"However, the conversion won't be 1:1 because there are more heuristics "
"working to decide which curves will be merged into one when the algorithm "
"reaches a T-junction among the visible colors. A hint about the heuristics "
"of this stage: You can't tune them."
msgstr ""
"Стандартний результат B-сплайнів буде плавним, тому що попередній результат "
"Вороного буде конвертовано в квадратичні криві Безьє (Bézier). Однак, "
"конвертація не буде 1:1, тому що там буде більше евристики при виборі "
"кривих, які будуть об'єднані в одну, коли алгоритм доходить до Т-подібного "
"з'єднання між видимими кольорами. Підказка стосовно евристики на цьому "
"етапі: її неможливо налаштувати."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:115
msgid ""
"The final stage of libdepixelize (currently not exportable by the Inkscape "
"GUI because of its experimental and incomplete state) is \"optimize curves\" "
"to remove the staircasing effect of the B-Spline curves. This stage also "
"performs a border detection technique to prevent some features from being "
"smoothed and a triangulation technique to fix the position of the nodes "
"after optimization. You should be able to individually disable each of these "
"features when this output leaves the \"experimental stage\" in libdepixelize "
"(hopefully soon)."
msgstr ""
"Фінальна стадія «libdepixelize» (наразі, неекспортовувана через графічний "
"інтерфейс «Inkscape» через її експериментальний і незавершений етап) – "
"«оптимізація кривих» для усунення ефекту сходинок на кривих B-сплайнів. Ця "
"стадія також застосовує метод визначення границь для запобігання "
"згладжування деяких елементів і метод тріангуляції для виправлення положення "
"вузлів після оптимізації. Кожну з цих опцій можна відключити окремо, коли "
"цей результат вийде з «експериментальної стадії» в "
"«libdepixelize» (сподіваємось, що незабаром)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:122
msgid ""
"The heuristics section in the gui allows you to tune the heuristics used by "
"libdepixelize to decide what to do when it encounters a 2x2 pixel block "
"where the two diagonals have similar colors. \"What connection should I keep?"
"\" is what libdepixelize asks. It tries to apply all heuristics to the "
"conflicting diagonals and keeps the connection of the winner. If a tie "
"happens, both connections are erased."
msgstr ""
"Евристичний розділ в графічному інтерфейсі дозволяє підлаштовувати "
"евристику, що використовується «libdepixelize» для вирішення, що робити, "
"коли вона зіштовхується з блоком пікселів 2×2, де дві діагоналі мають "
"подібні кольори. «Яке з'єднання слід зберегти?» – ось, що запитує "
"«libdepixelize». Вона намагається застосувати всю евристику до конфілктуючих "
"діагоналей і залишити з'єднання-переможець. Якщо трапляється вузол, то "
"обидва з'єднання видаляються."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:128
msgid ""
"If you want to analyze the effect of each heuristic and play with the "
"numbers, the best output is the Voronoi output. You can see more easily the "
"effects of the heuristics in the Voronoi output and when you are satisfied "
"with the settings you got, you can just change the output type to the one "
"you want."
msgstr ""
"Якщо хочете проаналізувати вплив кожної з евристик і погратися з числами, то "
"кращим результатом є результат Вороного. Простіше побачити вплив на "
"евристику в результаті Вороного і, коли налаштування задовольняють, можна "
"просто змінити тип результату на потрібний."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:133
msgid ""
"The image below has an image and the B-Splines output with only one of the "
"heuristics turned on for each try. Pay attention to the purple circles that "
"highlight the differences that each heuristic performs."
msgstr ""
"Зображення нижче містить зображення і результат B-сплайнів з лише однією "
"увімкненою евристикою для кожної спроби. Зверніть увагу на рожеві кола, які "
"виділяють різницю, що дає кожна з евристик."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:144
msgid ""
"For the first try (top image), we only enable the curves heuristic. This "
"heuristic tries to keep long curves connected together. You can notice that "
"its result is similar to the last image, where the sparse pixels heuristic "
"is applied. One difference is that its \"strength\" is more fair and it only "
"gives a high value to its vote when it's really important to keep these "
"connections. The \"fair\" definition/concept here is based on \"human "
"intuition\" given the pixel database analysed. Another difference is that "
"this heuristic can't decide what to do when the connections group large "
"blocks instead of long curves (think about a chess board)."
msgstr ""
"Для першої спроби (верхнє зображення), увімкнена лише евристика кривих. Вона "
"намагається зберегти довгі криві з'єднаними між собою. Можна помітити, що її "
"результат подібний до останнього зображення, де застосовувалась евристика "
"розкиданих пікселів. Різниця лише в тому, що її «сила» більш правдоподібна і "
"вона дає лише великі значення, коли дійсно важливо зберегти ці з'єднання. "
"Визначення/концепція «правдоподібна» базується на «людській інтуїції», даній "
"базі даних пікселів, що аналізуються. Інша відмінність – евристика не може "
"вирішити, що робити, коли з'єднання групують великі блоки замість довгих "
"кривих (уявіть шахову дошку)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:152
msgid ""
"For the second try (the middle image), we only enable the islands heuristic. "
"The only thing this heuristic does is trying to keep the connection that "
"otherwise would result in several isolated pixels (islands) with a constant "
"weight vote. This kind of situation is not as common as the kind of "
"situation handled by the other heuristics, but this heuristic is cool and "
"help to give still better results."
msgstr ""
"Для другої спроби (середнє зображення), активізуємо лише острівцеву "
"евристику. Єдине, що робить дана евристика – намагається зберегти з'єднання, "
"яке інакше видасть декілька ізольованих пікселів (острівців) зі сталою "
"вагою. Цей вид ситуації не є таким звичайним, як вид ситуації, що "
"оброблюється іншою евристикою, але ця евристика – чудова і все ще допомагає "
"отримати кращі результати."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:158
msgid ""
"For the third try (the bottom image), we only enable the sparse pixels "
"heuristic. This heuristic tries to keep the curves with the foreground color "
"connected. To find out what is the foreground color the heuristic analyzes a "
"window with the pixels around the conflicting curves. For this heuristic, "
"you not only tune its \"strength\", but also the window of pixels it "
"analyzes. But keep in mind that when you increase the window of pixels "
"analyzed the maximum \"strength\" for its vote will increase too and you "
"might want to adjust the multiplier for its vote. The original libdepixelize "
"author think this heuristic is too greedy and likes to use the \"0.25\" "
"value for its multiplier."
msgstr ""
"Для третьої спроби (нижнє зображення), активуємо лише евристику розкиданих "
"пікселів. Ця евристика намагається зберегти криві, об'єднаних основним "
"кольором. Щоб знайти основний колір, евристика аналізує вікно з пікселями "
"навколо конфліктних кривих. Для цієї евристики, підлаштовується не лише її "
"«сила», але й вікно з пікселями, що аналізуються. Але пам'ятайте, що "
"збільшуючи вікно з пікселями, що аналізуються, максимальна «сила» по її "
"рішенні також зросте і, можливо, потрібно буде налаштовувати множник її "
"рішення. Автор першої версії «libdepixelize» вважає цю евристику надто "
"жадібною і надає перевагу значення множника «0,25»."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:167
msgid ""
"Even if the results of the curves heuristic and the sparse pixels heuristic "
"give similar results, you may want to leave both enabled, because the curves "
"heuristic may give an extra safety that the important curves of contour "
"pixels won't be hampered and there are cases that can be only answered by "
"the sparse pixels heuristic."
msgstr ""
"Навіть, якщо результати евристики кривих і евристики розкиданих пікселів є "
"схожими, можливо, потрібно залишити активованими їх обох, тому що евристика "
"кривих може давати додаткову надійність, що для важливих кривих контурних "
"пікселів не буде перешкод і, що є випадки, які можна вирішити лише "
"евристикою розкиданих пікселів."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:172
msgid ""
"Hint: You can disable all heuristics by setting its multiplier/weight values "
"to zero. You can make any heuristic act against its principles using "
"negative values for its multiplier/weight values. Why would you ever want to "
"replace behaviour that was created to give better quality by the opposite "
"behaviour? Because you can... because you might want a \"artistic\" "
"result... whatever... you just can."
msgstr ""
"Порада: можна вимкнути всі евристики, виставивши значення множника/ваги на "
"нуль. Можна змусити будь-яку евристику діяти всупереч її законів, "
"використовуючи від'ємні значення її множника/ваги. Чому потрібно змінювати "
"поведінку того, що було створено, щоб давати результати кращої якості, на "
"зворотні? Тому що можна… тому що можливо потрібен «художній» результат… не "
"має значенні… просто можна."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:178
msgid ""
"And that's it! For this initial release of libdepixelize these are all the "
"options you got. But if the research of the original libdepixelize author "
"and its creative mentor succeeds, you may receive extra options that broaden "
"even yet the range of images for which libdepixelize gives a good result. "
"Wish them luck."
msgstr ""
"Ось і все! Для цього початкового випуску «libdepixelize» – це всі опції, що "
"є. Але, якщо дослідження автора першої версії «libdepixelize» та його "
"творчого керівника будуть вдалими, Ви, можливо, отримаєте додаткові опції, "
"що розширять ряд зображень, для яких «libdepixelize» дає чудові результати. "
"Побажаємо їм успіхів!"

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:183
msgid ""
"All images used here were taken from Liberated Pixel Cup to avoid copyright "
"problems. The links are:"
msgstr ""
"Всі зображення, використані тут, було взято з «Liberated Pixel Cup» для "
"уникнення проблем з авторськими правами. Посилання:"

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:188
msgid ""
"<ulink url=\"http://opengameart.org/content/memento\">http://opengameart.org/"
"content/memento</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:193
msgid ""
"<ulink url=\"http://opengameart.org/content/rpg-enemies-bathroom-tiles"
"\">http://opengameart.org/content/rpg-enemies-bathroom-tiles</ulink>"
msgstr ""

#. (itstool) path: Work/format
#: tracing-pixelart-f01.svg:49 tracing-pixelart-f02.svg:49
#: tracing-pixelart-f03.svg:49 tracing-pixelart-f04.svg:49
#: tracing-pixelart-f05.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#~ msgid "http://opengameart.org/content/memento"
#~ msgstr "http://opengameart.org/content/memento"

#~ msgid "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
#~ msgstr "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
